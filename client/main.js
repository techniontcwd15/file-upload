(function() {
  'use strict';

  fetch('/images-list')
    .then(res => res.json())
    .then(imagesNames => {
      imagesNames.forEach(imageName => {
        const imgEl = document.createElement('img');
        imgEl.src = 'images/' + imageName;

        document.body.appendChild(imgEl);
      });
    });
}());

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  images: string[] = [];

  constructor(private http: Http) {

  }

  refresh() {
    this.http.get('/images-list').map(res=>res.json())
    .subscribe(imagesNames => {
      this.images = imagesNames;
    });
  }

  sendFile(file) {
    const data = new FormData();
    data.append('image', file);

    this.http.post('/api/upload', data)
    .subscribe(res => {
      this.refresh();
    });
  }
}

const express = require('express');
const fileupload = require('express-fileupload');
const fs = require('fs');

const app = express();

app.use(fileupload());
// app.use(express.static('./client'))
app.use(express.static('./ng-upload/dist'))
app.use('/images', express.static('./images'))

app.post('/api/upload', async (req, res) => {
  await req.files.image.mv('./images/' + req.files.image.name);
  res.end();
});

app.get('/images-list', (req, res) => {
  fs.readdir('./images', (err, files) => {
    res.json(files);
  })

});

app.listen(3000);
